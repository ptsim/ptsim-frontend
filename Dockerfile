FROM node:13.12.0-alpine

WORKDIR /app

COPY build/dist/ /app/dist/
COPY build/server.js /app/server.js

ENTRYPOINT node server.js
