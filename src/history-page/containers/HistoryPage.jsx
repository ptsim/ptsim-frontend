import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

const HistoryPageWrapper = styled.div.attrs({ className: 'history-page-wrapper' })`
  padding: 30px;
  height: calc(100% - 60px);
  width: calc(100% - 60px);
`;

const PageName = styled.h1.attrs({ className: 'page-name' })`
  font-weight: 100;
  margin: 0 0 10px 0;
`;

const VisitsListWrapper = styled.div.attrs({ className: 'visits-list-wrapper' })`
  background: #fff;
  border: 1px solid #ddd;
  border-radius: 4px;
  padding: 20px;
  width: calc(80% - 40px);
  margin: 0 auto;
  line-height: 25px;
  box-shadow: 0 3px 8px rgba(0, 0, 0, 0.1);
`;

const VisitsList = styled.ul.attrs({ className: 'visits-list' })`
  margin: 0;
  padding: 0;
  list-style-type: none;
`;

const VisitsListItem = styled.li.attrs({ className: 'visits-list-item' })`
  display: grid;
  grid-template-columns: 10% repeat(3, 1fr);
  font-size: 12px;
  padding: 10px;
  width: calc(100% - 20px);
  border-bottom: 1px solid #ccc;
  transition: 0.2s;
  font-weight: 300;
`;

const VisitsListItemLink = styled(Link).attrs({ className: 'visits-list-item-link' })`
  display: grid;
  grid-template-columns: 10% repeat(3, 1fr);
  font-size: 12px;
  padding: 10px;
  width: calc(100% - 20px);
  border-bottom: 1px solid #ccc;
  transition: 0.2s;
  cursor: pointer;
  font-weight: 300;
  color: #000;
  text-decoration: none;

  &:nth-child(2n + 1) {
    background: #f9f9f9;
  }

  &:first-child {
    &:hover {
      background: #fff;
      cursor: default;
    }
  }

  &:hover {
    background: #eee;
  }

  &:last-child {
    border: none;
  }
`;

const DoctorDetails = styled.div.attrs({ className: 'doctor-details' })`
  display: flex;
`;

const DoctorName = styled.div.attrs({ className: 'doctor-name' })`
  font-size: 12px;
`;

const ListHeader = styled.div.attrs({ className: 'list-header' })`
  font-weight: 600;
`;

const UserCircle = styled.div.attrs({ className: 'user-circle' })`
  display: block;
  border-radius: 50%;
  width: 25px;
  height: 25px;
  line-height: 25px;
  margin-right: 10px;
  color: #ffffff;
  background: #2d4564;
  text-align: center;
  font-size: 9px;
  font-weight: 400;
`;

const VISITS = [
  {
    id: 1,
    doctor: {
      firstName: 'John',
      lastName: 'Doe',
    },
    date: new Date(),
    duration: 3123123,
  },
  {
    id: 2,
    doctor: {
      firstName: 'John',
      lastName: 'Doe',
    },
    date: new Date(),
    duration: 3123123,
  },
  {
    id: 3,
    doctor: {
      firstName: 'John',
      lastName: 'Doe',
    },
    date: new Date(),
    duration: 3123123,
  },
  {
    id: 4,
    doctor: {
      firstName: 'John',
      lastName: 'Doe',
    },
    date: new Date(),
    duration: 3123123,
  },
];

const monthNames = [
  'Styczeń',
  'Luty',
  'Marzec',
  'Kwiecień',
  'Maj',
  'Czerwiec',
  'Lipiec',
  'Sierpień',
  'Wrzesień',
  'Październik',
  'Listopad',
  'Grudzień',
];

const dayNames = ['Niedziela', 'Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek', 'Sobota'];

const addZeroBefore = n => (n < 10 ? '0' : '') + n;

const getDate = date =>
  addZeroBefore(date.getDate()) +
  ' ' +
  monthNames[date.getMonth()] +
  ' ' +
  addZeroBefore(date.getFullYear()) +
  ', ' +
  dayNames[date.getDay()] +
  ', ' +
  date.getHours() +
  ':' +
  date.getMinutes();

const HEADERS = ['ID wizyty', 'Doktor', 'Czas wizyty', 'Data wizyty'];

const HistoryPage = () => {
  const getCircleData = (firstName, lastName) => firstName.charAt(0) + lastName.charAt(0);

  const millisecondsToStr = milliseconds => {
    var temp = Math.floor(milliseconds / 1000);
    const years = Math.floor(temp / 31536000);
    if (years) return years + ' lata';

    const days = Math.floor((temp %= 31536000) / 86400);
    if (days) return days + ' dni';

    const hours = Math.floor((temp %= 86400) / 3600);
    if (hours) return hours + ' godziny';

    const minutes = Math.floor((temp %= 3600) / 60);
    if (minutes) return minutes + ' minuty';

    const seconds = temp % 60;
    if (seconds) return seconds + ' sekundy';

    return 'mniej niz sekunda';
  };

  return (
    <HistoryPageWrapper>
      <PageName>Historia przyjęć</PageName>
      <VisitsListWrapper>
        <VisitsList>
          <VisitsListItem>
            {HEADERS.map(item => (
              <ListHeader key={item}>{item}</ListHeader>
            ))}
          </VisitsListItem>
          {VISITS.map(visitItem => (
            <VisitsListItemLink to={`/visit/${visitItem.id}`} key={visitItem.id}>
              <div>{visitItem.id}</div>
              <DoctorDetails>
                <UserCircle>
                  {getCircleData(visitItem.doctor.firstName, visitItem.doctor.lastName)}
                </UserCircle>
                <DoctorName>
                  {`${visitItem.doctor.firstName} ${visitItem.doctor.lastName}`}
                </DoctorName>
              </DoctorDetails>
              <div>{millisecondsToStr(visitItem.duration)}</div>
              <div>{getDate(visitItem.date)}</div>
            </VisitsListItemLink>
          ))}
        </VisitsList>
      </VisitsListWrapper>
    </HistoryPageWrapper>
  );
};

export default HistoryPage;
