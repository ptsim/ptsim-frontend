import React, { useState } from 'react';
import styled from 'styled-components';

import inlineLoaderImage from 'images/inline-loader.svg';

const VisitPageWrapper = styled.div.attrs({ className: 'visit-page-wrapper' })`
  padding: 30px;
  height: calc(100% - 60px);
  width: calc(100% - 60px);
`;

const PageName = styled.h1.attrs({ className: 'page-name' })`
  font-weight: 100;
  margin: 0 0 10px 0;
`;

const LoaderImage = styled.img.attrs({ className: 'loader-image' })`
  width: 20%;
  margin: 0 auto;
  display: block;
`;

const QueueTitle = styled.div.attrs({ className: 'queue-title' })`
  font-weight: 200;
  text-align: center;
`;

const PatientsGrid = styled.div.attrs({ className: 'patients-grid' })`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  gap: 20px;
  width: 60%;
  margin: 15px auto 0 auto;
`;

const CurrentPatient = styled.div.attrs({ className: 'current-patient' })`
  text-align: right;
`;

const QueueNumber = styled.div.attrs({ className: 'queue-number' })`
  display: inline-block;
  padding: 20px;
  font-weight: bold;
  font-size: 30px;
  margin: 20px 0;
  color: #fff;
  background: rgba(45, 69, 100, 0.9);
  box-shadow: 0 3px 8px rgba(0, 0, 0, 0.5);
`;

const RegisterTitle = styled.div.attrs({ className: 'register-title' })`
  text-align: center;
  text-transform: uppercase;
  font-weight: 200;
  font-size: 16px;
  margin-bottom: 20px;
`;

const DontLeave = styled.div.attrs({ className: 'dont-leave' })`
  text-align: center;
  text-transform: uppercase;
  font-weight: 300;
  font-size: 11px;
  color: #fff;
  width: 80%;
  margin: 15px auto;
  padding: 10px 0;
  background: rgba(45, 69, 100, 0.9);
`;

const VisitRegisterWrapper = styled.div.attrs({ className: 'visit-register-wrapper' })`
  background: #fff;
  border: 1px solid #ddd;
  border-radius: 4px;
  padding: 20px;
  width: calc(80% - 40px);
  margin: 0 auto;
  line-height: 25px;
  box-shadow: 0 3px 8px rgba(0, 0, 0, 0.1);
`;

const InputLabel = styled.div.attrs({ className: 'input-label' })`
  font-size: 12px;
`;

const RegisteredWrapper = styled.div.attrs({ className: 'registered-wrapper' })`
  text-align: center;
`;

const ListTitle = styled.div.attrs({ className: 'list-title' })`
  font-size: 12px;
  font-weight: 400;
`;

const PatientsList = styled.ul.attrs({ className: 'patients-list' })`
  margin: 0;
  padding: 0;
  list-style-type: none;
  font-size: 12px;
  font-weight: 200;
  text-align: left;
`;

const PatientItem = styled.li.attrs({ className: 'patient-item' })``;

const Title = styled.div.attrs({ className: 'title' })`
  font-size: 12px;
  font-weight: 400;
`;

const Desc = styled.div.attrs({ className: 'desc' })`
  font-size: 12px;
  font-weight: 200;
`;

const StyledTextArea = styled.textarea.attrs({ className: 'styled-text-area' })`
  width: calc(100% - 40px);
  min-height: 100px;
  border-radius: 4px;
  border: 1px solid #ccc;
  padding: 20px;
  resize: none;
  outline: none;
`;

const RegisterButton = styled.button.attrs({ className: 'register-button' })`
  width: 100%;
  font-weight: 200;
  padding: 15px 10px;
  background: rgba(45, 69, 100, 0.9);
  color: #fff;
  border-radius: 4px;
  text-align: center;
  outline: none;
  border: none;
  font-size: 12px;
  transition: 0.2s;
  cursor: pointer;

  &:hover {
    opacity: 0.8;
  }

  &:disabled {
    opacity: 0.8;
    cursor: not-allowed;
  }
`;

const PATIENTS_LIST = [
  {
    id: 1,
    name: 'Jo...oe',
  },
  {
    id: 2,
    name: 'Jo...oe',
  },
  {
    id: 3,
    name: 'Jo...oe',
  },
  {
    id: 4,
    name: 'Jo...oe',
  },
  {
    id: 5,
    name: 'John Doe',
    currentUser: true,
  },
];

const VisitPage = () => {
  const [description, setDescription] = useState('');
  const [registered, setRegistered] = useState(false);
  const [lineLength, setLineLength] = useState(0);
  const [currentPatient, setCurrentPatient] = useState('Jo...oe');

  const onSend = () => {
    setRegistered(true);
    setLineLength(10);
  };

  return (
    <VisitPageWrapper>
      <PageName>Wizyta</PageName>
      <VisitRegisterWrapper>
        {registered ? (
          <RegisteredWrapper>
            <QueueTitle>Miejsce w kolejce</QueueTitle>
            <QueueNumber>{lineLength}</QueueNumber>
            <LoaderImage src={inlineLoaderImage} />
            <DontLeave>Nie opuszczaj tej strony aby zachować miejsce w kolejce!</DontLeave>
            <PatientsGrid>
              <PatientsList>
                <ListTitle>Lista pacjentów w kolejce:</ListTitle>
                {PATIENTS_LIST.map(patient => (
                  <PatientItem
                    style={
                      patient.currentUser ? { fontWeight: 'bold', textDecoration: 'underline' } : {}
                    }
                  >{`${patient.id}. ${patient.name}`}</PatientItem>
                ))}
              </PatientsList>
              <CurrentPatient>
                <Title>Aktualna wizyta:</Title>
                <Desc>Pacjent: {currentPatient}</Desc>
                <Desc>Czas trwania: 0:12</Desc>
              </CurrentPatient>
            </PatientsGrid>
          </RegisteredWrapper>
        ) : (
          <div>
            <RegisterTitle>Rejestracja</RegisterTitle>
            <InputLabel>Opisz swoją przypadłość poniżej:</InputLabel>
            <StyledTextArea
              value={description}
              onChange={evt => setDescription(evt.target.value)}
            />
            <RegisterButton onClick={onSend} disabled={description.length === 0}>
              Zarejestruj się
            </RegisterButton>
          </div>
        )}
      </VisitRegisterWrapper>
    </VisitPageWrapper>
  );
};

export default VisitPage;
