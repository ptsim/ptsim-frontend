import React from 'react';
import styled from 'styled-components';

import WelcomeBackBadge from 'dashboard-page/components/WelcomeBackBadge';
import DashboardStats from 'dashboard-page/components/DashboardStats';
import DashboardGoToPages from 'dashboard-page/components/DashboardGoToPages';
import DashboardChart from 'dashboard-page/components/DashboardChart';
import UserInfo from 'dashboard-page/components/UserInfo';

const DashboardPageWrapper = styled.div.attrs({ className: 'dashboard-page-wrapper' })`
  display: grid;
  grid-template-columns: 22fr 8fr;
  padding: 30px;
  height: calc(100% - 60px);
  width: calc(100% - 60px);
`;

const DashboardLeft = styled.div.attrs({ className: 'dashboard-left' })`
  padding: 0 20px;
`;

const DashboardRight = styled.div.attrs({ className: 'dashboard-right' })``;

const DashboardPage = () => (
  <DashboardPageWrapper>
    <DashboardLeft>
      <WelcomeBackBadge username="John Doe" />
      <DashboardStats />
      <DashboardGoToPages />
      <DashboardChart />
    </DashboardLeft>
    <DashboardRight>
      <UserInfo />
    </DashboardRight>
  </DashboardPageWrapper>
);

export default DashboardPage;
