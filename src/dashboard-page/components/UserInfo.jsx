import React from 'react';
import styled from 'styled-components';

import UserInfoHeader from 'dashboard-page/components/UserInfoHeader';
import SmallCalendar from 'dashboard-page/components/SmallCalendar';

const UserInfoWrapper = styled.div.attrs({ className: 'user-info-wrapper' })`
  background: #fff;
  border: 1px solid #ddd;
  padding: 30px;
`;

const UserDescription = styled.p.attrs({ className: 'user-description' })`
  padding-top: 10px;
  border-top: 2px solid #f4f4f4;
  font-size: 12px;
  font-family: 'Roboto-Light', sans-serif;
`;

const UserInfo = () => (
  <UserInfoWrapper>
    <UserInfoHeader firstName="John" lastName="Doe" />
    <SmallCalendar />
  </UserInfoWrapper>
);

export default UserInfo;
