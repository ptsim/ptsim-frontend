import React from 'react';
import styled from 'styled-components';
import { Line as LineChart } from 'react-chartjs-2';

const DashboardChartWrapper = styled.div.attrs({ className: 'dashboard-chart-wrapper' })`
  background: #ffffff;
  border: 1px solid #ddd;
  box-shadow: 0 3px 8px rgba(0, 0, 0, 0.1);
  padding: 20px;
  width: calc(100% - 40px);
  margin-top: 20px;
`;

const ChartName = styled.div.attrs({ className: 'chart-name' })`
  text-transform: uppercase;
  font-weight: 300;
  font-size: 12px;
  margin-bottom: 20px;
  border-bottom: 1px solid #ccc;
  padding-bottom: 5px;
  display: inline-block;
`;

const CHART_DATA = {
  labels: ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec', 'Lipiec'],
  datasets: [
    {
      showLabel: false,
      backgroundColor: 'rgba(45, 69, 100, 0.9)',
      strokeColor: 'rgba(45, 69, 100, 0.9)',
      pointColor: 'rgba(45, 69, 100, 0.9)',
      pointStrokeColor: '#fff',
      pointHighlightFill: '#fff',
      pointHighlightStroke: 'rgba(45, 69, 100, 0.9)',
      data: [65, 59, 80, 81, 56, 55, 40],
    },
  ],
};

const options = {
  legend: {
    display: false,
  },
  scaleShowGridLines: true,
  scaleGridLineColor: 'rgba(0,0,0,.9)',
  scaleGridLineWidth: 1,
  scaleShowHorizontalLines: true,
  scaleShowVerticalLines: true,
  bezierCurve: true,
  bezierCurveTension: 0.4,
  pointDot: true,
  pointDotRadius: 4,
  pointDotStrokeWidth: 1,
  pointHitDetectionRadius: 20,
  datasetStroke: true,
  datasetStrokeWidth: 2,
  datasetFill: true,
};

const DashboardChart = () => (
  <DashboardChartWrapper>
    <ChartName>Liczba wizyt w ostatnich miesiącach</ChartName>
    <LineChart data={CHART_DATA} options={options} height={50} />
  </DashboardChartWrapper>
);

export default DashboardChart;
