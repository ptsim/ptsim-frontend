import React from 'react';
import { Route, Switch } from 'react-router-dom';

import getPath from 'common/utils/path';
import makeLoadable from 'common/utils/loadable';
import NotFoundPage from 'common/components/NotFoundPage';
import Layout from 'common/components/layout/Layout';
// import AuthService from 'AuthService';

export const DASHBOARD_PATH = getPath('/');
export const VISIT_PATH = getPath('/visit');
export const VISIT_DETAILS_PATH = getPath('/visit/:visitId');
export const HISTORY_PATH = getPath('/history');

export const LoadableDashboardPage = makeLoadable(() =>
  import('dashboard-page/containers/DashboardPage'),
);

export const LoadableVisitPage = makeLoadable(() =>
  import('visit-page/containers/VisitPage'),
);

export const LoadableHistoryPage = makeLoadable(() =>
  import('history-page/containers/HistoryPage'),
);

export const LoadableVisitDetailsPage = makeLoadable(() =>
  import('visit-details-page/containers/VisitDetailsPage'),
);

const RootRouter = () => (
  <Switch>
    {/* <AuthService> */}
    <Layout>
      <Route exact path={DASHBOARD_PATH} component={LoadableDashboardPage} />
      <Route exact path={VISIT_PATH} component={LoadableVisitPage} />
      <Route exact path={HISTORY_PATH} component={LoadableHistoryPage} />
      <Route exact path={VISIT_DETAILS_PATH} component={LoadableVisitDetailsPage} />
    </Layout>
    {/* </AuthService> */}
  </Switch>
);

export default RootRouter;
