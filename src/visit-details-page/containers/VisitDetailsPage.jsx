import React from 'react';
import styled from 'styled-components';

const VisitDetailsPageWrapper = styled.div.attrs({ className: 'visit-details-page-wrapper' })`
  padding: 30px;
  height: calc(100% - 60px);
  width: calc(100% - 60px);
`;

const PageName = styled.h1.attrs({ className: 'page-name' })`
  font-weight: 100;
  margin: 0 0 10px 0;
`;

const VisitDetailsWrapper = styled.div.attrs({ className: 'visit-details-wrapper' })`
  background: #fff;
  border: 1px solid #ddd;
  border-radius: 4px;
  padding: 20px;
  width: calc(60% - 40px);
  margin: 0 auto;
  line-height: 25px;
  box-shadow: 0 3px 8px rgba(0, 0, 0, 0.1);
`;

const VisitDetailsPage = () => (
  <VisitDetailsPageWrapper>
    <PageName>Wizyta - ID 2</PageName>
    <VisitDetailsWrapper>details</VisitDetailsWrapper>
  </VisitDetailsPageWrapper>
);

export default VisitDetailsPage;
