module.exports = {
  extends: ['airbnb', 'prettier', 'plugin:react-hooks/recommended'],
  parser: 'babel-eslint',
  env: {
    browser: true,
    'jest/globals': true,
  },
  settings: {
    'import/resolver': {
      node: {
        paths: ['src'],
      },
    },
  },
  plugins: ['jest', 'prettier'],
  rules: {
    'linebreak-style': ['error', 'windows'],
    'linebreak-style': ['error', 'unix'],
    'react-hooks/exhaustive-deps': 0,
    'jsx-a11y/label-has-for': 0,
    'jsx-a11y/click-events-have-key-events': 0,
    'jsx-a11y/no-static-element-interactions': 0,
    'prettier/prettier': 'error',
    'jsx-a11y/anchor-is-valid': [
      'error',
      {
        components: ['Link'],
        specialLink: ['to'],
      },
    ],
  },
};
