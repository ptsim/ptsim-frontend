# PTSIM - Frontend

PTSIM - Frontend coponent

### Prerequisites
  - Node (version >= 10)
  - yarn

### Installation
In order to install needed packages you need to execute command 

```sh
$ yarn install
```

### Starting dev server
In order to run development webpack server you need to execute command 

```sh
$ yarn start
```

You can reach a website on https://call-network:3000


### Running tests
In order to run tests you need to execute command 

```sh
$ yarn test
```
